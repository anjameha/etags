;; etags.el -- A tag database

;;; License
;;
;; Copyright 2018 Andrew JM Hall

;;   Licensed under the Apache License, Version 2.0 (the "License");
;;   you may not use this file except in compliance with the License.
;;   You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;;; Commentary:
;; The jumping off point to investigate semantic file systems within an Emacs context


(require 'cl)
(require 's)
(require 'emacsql-sqlite)

;;; Code:

(defvar etags-debug nil
  "If non-nil log messages.")

(setq etags-debug t)

(defcustom etags-root (expand-file-name "etags" user-emacs-directory)
  "*Directory variable from which all other etags file variables are derived."
  :group 'etags)

(defconst etags-default-db-name "etags.sqlite"
  "Default name of the sqlite database file.")

;;(defcustom etags-db-name "etags.sqlite"
(defcustom etags-db-name etags-default-db-name
  "Name of the sqlite database file."
  :group 'etags)

(unless (file-directory-p etags-root)
  (make-directory etags-root t))

(defvar etags-db (emacsql-sqlite (expand-file-name etags-db-name etags-root) (when etags-debug :debug))
  "Variable for the etags db connection.")

(defvar etags-log-file (expand-file-name "etags.log" etags-root)
  "Path to the log file.")


(defun etags-log (format-string &rest args)
  "Insert the FORMAT-STRING formatted with ARGS into the log buffer."
  (when etags-debug
    (with-current-buffer (get-buffer-create "*etags-log*")
      (goto-char (point-max))
      (insert (format "%s:\n" (current-time-string)))
      (let* ((msg (with-temp-buffer
                    (insert (format "%s\n"
                                    (apply 'format format-string args)))
                    (fill-region (point-min) (point-max))
                    (indent-region (point-min) (point-max) 4)
                    (buffer-string))))
        (insert msg)
        (message (s-trim msg))))))

(etags-log "Started etags")

(defun etags-verify-schema ()
  "Create or verify the database schema."

  ;; https://www.sqlite.org/pragma.html#pragma_foreign_keys
  (emacsql etags-db [:PRAGMA (= foreign_keys 1)])

;; [[http://www.sqlitetutorial.net/sqlite-autoincrement/][SQLite AUTOINCREMENT : Why You Should Avoid Using It]]

  ;; create tables and indices
  (emacsql etags-db [:create-table :if :not :exists tags
                     ([(tag_pk integer :primary-key)
                       (name text :unique :not :null)])])

  (emacsql etags-db [:create-index :if :not :exists tag_names :on tags [name]])

  (emacsql etags-db [:create-table :if :not :exists files
                     ([(file_pk integer :primary-key)
                       (path text :unique :not :null)])])

  (emacsql etags-db [:create-index :if :not :exists file_paths :on files [path]])

  (emacsql etags-db [:create-table :if :not :exists tag_files
                     ([(tagfile_pk integer :primary-key)
                       (tag_fk integer :not :null)
                       (file_fk integer :not :null)]
                      (:unique [tag_fk file_fk])
                      (:foreign-key [tag_fk] :references tags [tag_pk]
                                    :on-delete :cascade)
                      (:foreign-key [file_fk] :references files [file_pk]
                                    :on-delete :cascade))])

  (emacsql etags-db [:create-index :if :not :exists tagfiles_tags :on tag_files [tag_fk]])

  (emacsql etags-db [:create-index :if :not :exists tagfiles_files :on tag_files [file_fk]]))

;; create the database schema if we need to.
(progn
  (etags-verify-schema)
  )


(defun etags-connect ()
  "Make sure we're connected."
  (unless (and etags-db (emacsql-live-p etags-db))
    (setq etags-db (emacsql-sqlite (expand-file-name etags-db-name etags-root)
                                   (when etags-debug :debug)))))


(defun etags-get-tag-id (tag-name)
  "Return the PK corresponding to TAG-NAME.
Adds tag to the database if it doesn't exist."
  (etags-connect)
  (or
   ;; this is a tag in the database
   (caar (emacsql etags-db [:select tag_pk :from tags
                            :where (= name $s1)] tag-name))
   ;; no tag found, add one and get the id
   (prog2
       (etags-log (format "Creating new tag %S" tag-name))
       (emacsql-with-transaction etags-db
         (emacsql etags-db [:insert :into tags [name] :values [$s1]]
                  tag-name)
         (caar (emacsql etags-db [:select (funcall last-insert-rowid)])))
  )))


(defun etags-delete-tag (tag-name)
  "Delete a tag identified by TAG-NAME."
  (etags-connect)
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:delete :from tags :where (= name $s1)] tag-name)))


(defun etags-rename-tag (tag-id name)
  "Set a tag's NAME identified by TAG-ID."
  (etags-connect)
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:update tags :set (= name $s1) :where (= tag_pk $s2)]
           name tag-id)))


(defun etags-get-file-mime-type (filepath)
  "Return FILEPATH mime type."
  (if (file-exists-p filepath) ;; file(1) doesn't fail if the target is missing
      (if (file-readable-p filepath)
          (with-temp-buffer
            (let ((exit (call-process "file" nil t nil "--brief" "--mime-type" filepath)))
              ;; Remove the final newline if any
              (when (eq (char-before) ?\n)
                (delete-char -1))
              (if (zerop exit)
                  (buffer-string)
                (etags-log (format "Command failed %S" buffer-string)))))
        (etags-log (format "Executing file(1) on %S. Permission denied" filepath)))
    (etags-log (format "Executing file(1) on %S. No such file or directory" filepath))))


(defun etags-get-file-id (filepath)
  "Return the PK corresponding to FILEPATH.
Adds file to the database if it doesn't exist."
  (etags-connect)
  (or
   ;; this is a file in the database
   (caar (emacsql etags-db [:select file_pk :from files :where (= path $s1)]
                  filepath))
   ;; no file found, add one and get the id
   (prog2
       (etags-log (format "Inserting file %S" filepath))
       (emacsql-with-transaction etags-db
         (emacsql etags-db [:insert :into files [path] :values [$s1]]
                  filepath)
         (caar (emacsql etags-db [:select (funcall last-insert-rowid)])))
     )))


(defun etags-delete-file (filepath)
  "Deletes a file named by FILEPATH from the database."
  (etags-connect)
  (etags-log (format "Removing %s from the db" filepath))
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:delete :from files :where (= path $s1)] filepath)))


(defun etags-tag-file (tag filepath)
  "Tags a file identified by FILEPATH with TAG."
  (etags-connect)
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:insert :or :ignore :into tag_files [tag_fk file_fk]
                               :values [$s1 $s2]]
             (etags-get-tag-id tag) (etags-get-file-id filepath)))
  (etags-log (format "Tagged %S as %S" filepath tag)))


(defun etags-untag-file (tag filepath)
  "Untags a file identified by FILEPATH with TAG."
  (etags-log (format "Untagging %S from %S" tag filepath))
  (etags-connect)
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:delete :from tag_files
                               :where (and (= file_fk $s1) (= tag_fk $s2))]
             (etags-get-file-id filepath) (etags-get-tag-id tag))
      ))


(defun etags-clean-db ()
  "Database garbage collection."
  (interactive)

  ;;Remove entries from the database where the file doesn't exist.
  (loop for (filepath) in (emacsql etags-db [:select :distinct [path] :from files])
        unless (file-exists-p filepath)
        do
        (etags-log "%S was not found. Removing it." filepath)
        (etags-delete-file filepath))

  ;; Remove files that have no tags.
  (etags-connect)
  (emacsql-with-transaction etags-db
    (emacsql etags-db [:delete :from files :where [file_pk] :in
                               [:select [file_pk] :from files
                                        :except :select [file_fk] :from tag_files]]))

  ;; Remove tags that have no files.
  (if (yes-or-no-p "Do you want to remove orphaned tags?")
      (progn
        (emacsql-with-transaction etags-db
        (emacsql etags-db [:delete :from tags :where [tag_pk] :in
                           [:select [tag_pk] :from tags
                            :except :select [tag_fk] :from tag_files]])))))


(defcustom etags-before-tag-hook nil
  "Hooks that are run before tags of a file are modified.
'tags' will contain the tags that are about to be added or removed as
a list of strings of the form \"+TAG\" or \"-TAG\".
'filename' will be the full path to the file that is about to be tagged"
  :type 'hook
  :group 'etags-hooks)


(defcustom etags-after-tag-hook nil
  "Hooks that are run after tags of a message are modified.
'tags' will contain the tags that are about to be added or removed as
a list of strings of the form \"+TAG\" or \"-TAG\".
'filename' will be the full path to the file that is about to be tagged"
  :type 'hook
  :group 'etags-hooks)


(defvar etags-read-tag-changes-history nil
  "Variable to store minibuffer history for `etags-read-tag-changes' function."
  )


(defun etags-tag-completions (&optional search-terms)
  "Return list of tag names matching SEARCH-TERMS."
  (if (null search-terms)
      (setq search-terms "%"))

  (etags-connect)
  (let (tag-list (list))
    (loop for (tag-name) in (emacsql etags-db [:select :distinct [name] :from tags
                                               :inner :join tag_files :on (= tags:tag_pk tag_files:tag_fk)
                                               :inner :join files :on (= tag_files:file_fk files:file_pk)
                                               :where (like files:path $s1)] search-terms)
          do
          (push tag-name tag-list)
          )
    (reverse tag-list))
  )


(defun etags-read-tag-changes (&optional initial-input &rest search-terms)
  "TODO: Write some documentation about this function and INITIAL-INPUT SEARCH-TERMS."
  (let* ((all-tag-list (etags-tag-completions))
         (add-tag-list (mapcar (apply-partially 'concat "+") all-tag-list))
         (remove-tag-list (mapcar (apply-partially 'concat "-")
                                  (if (null search-terms)
                                      all-tag-list
                                    (etags-tag-completions search-terms))))
         (tag-list (append add-tag-list remove-tag-list))
         (crm-separator " ")
         ;; By default, space is bound to "complete word" function.
	     ;; Re-bind it to insert a space instead.  Note that <tab>
         ;; still does the completion.
         (crm-local-completion-map
          (let ((map (make-sparse-keymap)))
            (set-keymap-parent map crm-local-completion-map)
            (define-key map " " 'self-insert-command)
            map)))
    (delete "" (completing-read-multiple "Tags (+add -drop): "
                                         tag-list nil nil initial-input
                                         'etags-read-tag-changes-history))))


(defun etags-tag (filename &optional tag-changes)
  "Add/remove tags in TAG-CHANGES to the file matching FILENAME.
FILENAME should be a string containing the fulll path to the file.
TAG-CHANGES can take multiple forms.  If TAG-CHANGES is a list of
strings of the form \"+tag\" or \"-tag\" then those are the tag
changes applied.  If TAG-CHANGES is a string then it is
interpreted as a single tag change.  If TAG-CHANGES is the string
\"-\" or \"+\", or null, then the user is prompted to enter the
tag changes.
Note: Other code should always use this function alter tags of
messages instead of running (notmuch-call-notmuch-process \"tag\" ..)
directly, so that hooks specified in notmuch-before-tag-hook and
notmuch-after-tag-hook will be run."
  ;; Perform some validation
  (if (string-or-null-p tag-changes)
      (if (or (string= tag-changes "-") (string= tag-changes "+") (null tag-changes))
	  (setq tag-changes (etags-read-tag-changes tag-changes filename))
	(setq tag-changes (list tag-changes))))
  (mapc (lambda (tag-change)
	  (unless (string-match-p "^[-+]\\S-+$" tag-change)
	    (error "Tag must be of the form `+this_tag' or `-that_tag'")))
	tag-changes)
  (unless (null tag-changes)
    (run-hooks 'etags-before-tag-hook)
    (loop for tag in tag-changes
          do
          (if (string-prefix-p "+" tag)
              (etags-tag-file (substring tag 1) filename)
            (etags-untag-file (substring tag 1) filename))
          )

    ;; Add additional mime tag
    (let ((mime-tag (etags-get-file-mime-type filename)))
      (unless (null mime-tag)
        (etags-tag-file mime-tag filename)))

    (run-hooks 'etags-after-tag-hook)
    )
  ;; in all cases we return tag-changes as a list
tag-changes)


(defun etags-tag-current-buffer (&optional tag-changes)
  "Apply tags to the current buffer if it is visiting a file.
TAG-CHANGES are the tags."
  (interactive)
  (if buffer-file-name
      (etags-tag(buffer-file-name tag-changes))
  ))


(defun etags-tag-marked-files (&optional tag-changes)
  "Apply tags to marked files.  TAG-CHANGES are the tags."
  (interactive)
  (loop for file in (dired-get-marked-files)
        do
        (setq tag-changes (etags-tag file tag-changes))))


(defun etags-untag-marked-files ()
  "Remove marked files from the tag db."
  (interactive)
  (loop for file in (dired-get-marked-files)
        do
        (etags-delete-file file)))


(defun etags-open-file ()
  "Open a file in `etags-db’ with completion."
  (interactive)
  (etags-connect)
  (find-file (completing-read "File: " (mapcar 'car (emacsql etags-db [:select [path]
                                                                               :from files
                                                                               :order :by path])))))

(defun etags-match-tags (tag-query &optional all)
  "Retrieve files matching the TAG-QUERY.
The tag query will OR the +tags if ALL is false,
AND them if ALL is true and exclude the -tags."

  (let (locations (list))
    (unless (null tag-query)
      (let ((inc-tags (list)) (exc-tags (list)))
        (loop for tag in tag-query
              do
              (if (string-prefix-p "+" tag)
                  (push (substring tag 1) inc-tags)
                (push (substring tag 1) exc-tags)))

        ;; Do we have any search criteria?
        (if (> (length inc-tags) 0)
            (progn
              (etags-log (format "Matching %s %s (%s), excluding %s..."
                                 (if all "ALL" "ANY")
                                 inc-tags (length inc-tags) exc-tags))

              ;; select f.path, t.name from files f
              ;; inner join tag_files tf on f.file_pk = tf.file_fk
              ;; inner join tags t on t.tag_pk = tf.tag_fk
              ;; where t.name in ('"tag_1"', '"tag_2"')
              ;; and t.name not in ('"people"')
              ;; group by f.path
              ;; having count(distinct t.tag_pk) = 2

              (etags-connect)
              (loop for (filepath) in

                    (if all
                        (emacsql etags-db [:select f:path :from (as files f)
                                           :inner :join (as tag_files tf) :on (= f:file_pk tf:file_fk)
                                           :inner :join (as tags t) :on (= t:tag_pk tf:tag_fk)
                                           :where (in t:name $v1) :and :not (in t:name $v2)
                                           :group :by f:path
                                           :having :count (= [:distinct t:tag_pk] $s3)]
                                 (vconcat inc-tags) (vconcat exc-tags) (length inc-tags))
                      (emacsql etags-db [:select f:path :from (as files f)
                                         :inner :join (as tag_files tf) :on (= f:file_pk tf:file_fk)
                                         :inner :join (as tags t) :on (= t:tag_pk tf:tag_fk)
                                         :where (in t:name $v1) :and :not (in t:name $v2)]
                               (vconcat inc-tags) (vconcat exc-tags)))
                    do
                    (progn
                      (push filepath locations)
                      (when etags-debug
                        (etags-log (format "Retrieved path %S" filepath)))
                      )
                      )))))
    locations)
  )


(defun etags-search-tags-all (&optional tag-query)
  "Return files from a TAG-QUERY."
  (interactive)
  (if (string-or-null-p tag-query)
      (if (or (string= tag-query "-") (string= tag-query "+") (null tag-query))
	  (setq tag-query (etags-read-tag-changes tag-query))
	(setq tag-query (list tag-query))))
  (mapc (lambda (query)
	  (unless (string-match-p "^[-+]\\S-+$" query)
	    (error "Tag must be of the form `+this_tag' or `-that_tag'")))
	tag-query)
  (unless (null tag-query)
    (let ((locations (etags-match-tags tag-query t)))
      (when (> (length locations) 0)
        (dired (cons "Files matching tag query." locations))))
    )
  )


(defun etags-search-tags-any (&optional tag-query)
  "Return files from a TAG-QUERY."
  (interactive)
  (if (string-or-null-p tag-query)
      (if (or (string= tag-query "-") (string= tag-query "+") (null tag-query))
	  (setq tag-query (etags-read-tag-changes tag-query))
	(setq tag-query (list tag-query))))
  (mapc (lambda (query)
	  (unless (string-match-p "^[-+]\\S-+$" query)
	    (error "Tag must be of the form `+this_tag' or `-that_tag'")))
	tag-query)
  (unless (null tag-query)
    (let ((locations (etags-match-tags tag-query nil)))
      (when (> (length locations) 0)
        (dired (cons "Files matching tag query." locations))))
    )
  )


(defun etags-custom-database ()
  "Switch database instances."
  (interactive)
  (setq etags-db-name (read-file-name "Enter database filename: "))
  (emacsql-close etags-db)
  (setq etags-db (emacsql-sqlite etags-db-name (when etags-debug :debug)))
  (etags-connect)
  (etags-verify-schema)
  (etags-log (format "Switched db to %S" etags-db-name)))


(defun etags-default-database ()
  "Revert back to the default db instance."
  (interactive)
  (if (not (string-equal etags-db-name etags-default-db-name))
      (progn
        (emacsql-close etags-db)

        (setq etags-db-name etags-default-db-name)
        (setq etags-db (emacsql-sqlite (expand-file-name etags-db-name etags-root)
                                       (when etags-debug :debug)))
        (etags-connect)
        (etags-verify-schema)
        (etags-log (format "Switched db to %S" (expand-file-name etags-db-name etags-root))))))


(defun etags-which-database ()
  "Show the current database in use."
  (interactive)

  (message "Current database is %S"
           (if (string-equal etags-db-name etags-default-db-name)
               (expand-file-name etags-db-name etags-root)
             etags-db-name))
  )



;; TODO: see https://gitlab.com/anjameha/etags/issues for bugs/enhancements
;; * End
(provide 'etags)

;;; etags.el ends here
