# etags (Emacs Tags)

I've been wanting to write a tag based/semantic file system but I've never really found the time. However since I started using Emacs, I figured that it would make a great starting point. Allowing me to tag files and search for them with Emacs. This is the initial commit, and it's still a work in progress. More documentation to follow in a few days.
